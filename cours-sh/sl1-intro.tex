\incdir {inc1-intro}

\titreA {Introduction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Objectif}
    Objectifs~:

    \begin {itemize}
	\item Délimiter le périmètre d'un SE
	    \begin {itemize}
		\item où est la frontière entre SE et architecture des
		    ordinateurs ?
		\item où est la frontière entre SE et applications ?
	    \end {itemize}
	\item Avoir une vue d'ensemble des abstractions offertes
	    par un SE
	    \begin {itemize}
		\item processus, fichiers, périphériques, utilisateurs, etc.
		\item pourquoi ces abstractions ?
	    \end {itemize}
	\item Comprendre les différences entre systèmes
	    \begin {itemize}
		\item héritage d'Unix et systèmes propriétaires
		\item norme POSIX
	    \end {itemize}
	\item Connaître les bases de la ligne de commande
	    \begin {itemize}
		\item le shell comme langage de programmation
	    \end {itemize}
	\item Exercices pratiques
	    \begin {itemize}
		\item bases de l'administration d'un serveur Unix
	    \end {itemize}
    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Qu'est-ce qu'un système d'exploitation ?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Des premiers ordinateurs aux systèmes d'exploitation}

\begin {frame} {Qu'est-ce qu'un système d'exploitation ?}
    Comment définir ce qu'est un système d'exploitation (SE) ?

    \begin {itemize}
	\item Facile, eh, m'sieur : Windows, Linux, FreeBSD, etc. !
    \end {itemize}

    \medskip

    Est-ce aussi simple~?

    \begin {itemize}
	\item si Linux est un SE, que sont Debian, Ubuntu, etc. ?
	\item et Android, iOS ?
	\item et QNX, RTLinux, VxWorks ?
	\item et Contiki, TinyOS ?
    \end {itemize}

    Et, au fait...

    \begin {itemize}
	\item est-ce qu'une box d'accès à l'Internet a un SE ?
	\item est-ce qu'une montre connectée a un SE ?
	\item est-ce qu'un thermostat de radiateur a un SE ?
	\item est-ce qu'une voiture a un SE ?
    \end {itemize}

\end {frame}

\begin {frame} {Qu'est-ce qu'un système d'exploitation ?}

    Pour définir ce qu'est un SE, il faut examiner l'histoire

    \medskip

    \implique quels besoins / problèmes doit résoudre un SE ?

\end {frame}

\begin {frame} {Historique -- Les premiers ordinateurs}

    Exemple : ENIAC (1946)

    \begin {center}
	\figcredit{eniac}{.8} {ENIAC -- {\ccpd} U.S. Army \href{https://commons.wikimedia.org/wiki/File:Eniac.jpg}{Wikimedia Commons}}
    \end {center}

\end {frame}

\begin {frame} {Historique -- Les premiers ordinateurs}

    Sur ces ordinateurs~:

    \begin {itemize}
	\item programmer : câbler des connexions entre les unités

	\item résultats : à consulter sur des indicateurs lumineux

    \end {itemize}

    L'ordinateur~:
    \begin {itemize}
	\item est très onéreux (généralement un seul exemplaire)
	\item est très difficile à programmer (6 programmeuses à l'origine
	    sur l'ENIAC, la programmation dure plusieurs semaines)
	\item se programme par câblage physique
	\item n'exécute qu'un seul programme à la fois
	\item n'a pas ou peu de périphériques
    \end {itemize}

    \implique pas de système d'exploitation

\end {frame}


\begin {frame} {Historique -- Démarrage aux clefs}

    Étape suivante~:

    \begin {center}
	\figcredit{pdp1}{.5}{PDP-1 control board -- {\ccby} fjarlq / Matt \href{https://commons.wikimedia.org/wiki/File:PDP-1_control_board.jpg}{Wikimedia Commons}}
    \end {center}

    \begin {itemize}
	\item panneau de commande de l'ordinateur
	\item saisie du programme en mémoire avec des interrupteurs
	\item lancement du programme avec un interrupteur
	\item lecture du résultat avec les indicateurs lumineux
    \end {itemize}
    \implique pas de système d'exploitation

\end {frame}


\begin {frame} {Historique -- Carte perforée}
    La carte perforée (début des années 1950)

    \begin {minipage} [c] {.40\linewidth}
	\figcredit{carte-perfo}{}{{\ccbysa} Arnold Reinhold \href{https://commons.wikimedia.org/wiki/File:IBM1130CopyCard.agr.jpg}{Wikimedia Commons}}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.58\linewidth}
	\figcredit{ibm704}{} {IBM 704 avec lecteur de cartes perforées -- {\ccpd} NASA \href{https://commons.wikimedia.org/w/index.php?curid=6455009}{Wikimedia Commons}}
    \end {minipage}

    \begin {itemize}
	\item périphérique d'entrée des programmes (et des données)
	\item programme en mémoire (morte) pour démarrer le lecteur,
	    stocker le programme en mémoire, et lancer son exécution
    \end {itemize}
\end {frame}

\begin {frame} {Historique -- Moniteur}

    Carte perforée \implique entrée automatisée des programmes

    \medskip

    Cependant :

    \begin {itemize}
	\item intervention d'un opérateur humain pour :
	    \begin {itemize}
		\item placer le bac de cartes dans le lecteur
		\item lancer la lecture
		\item attendre la fin du programme
		\item récupérer les résultats (sur l'imprimante)
	    \end {itemize}

	\item l'ordinateur est très onéreux : toute minute de calcul
	    perdue coûte cher !

    \end {itemize}

    D'où : programmes « \textbf {moniteurs} » résidant en mémoire

    \begin {itemize}
	\item toujours un seul programme à la fois
	\item automatisation du passage des différents programmes
	\item traitement par « lot » (de cartes) \implique
	    \textbf {batch processing}
	\item accès facilité aux périphériques
	\item premiers embryons de systèmes d'exploitation
    \end {itemize}

\end {frame}

\begin {frame} {Historique -- Spooling}
    Ordinateur onéreux \implique mieux le rentabiliser ?

    \begin {itemize}
	\item certains périphériques (lecteur de cartes perforées,
	    imprimante) sont lents

	    \fig {spool1} {.9}


	\item peut-on lancer un calcul pendant que les périphériques
	    lents travaillent ?

	    \fig {spool2} {.9}

    \end {itemize}

\end {frame}

\begin {frame} {Historique -- Spooling}
    Exemple~: IBM 7094 (1963)

    \begin {center}
	\figcredit{spool-tanenb}{.8} {Extrait de Tanenbaum, A.S., Woodhull A.S.,
		« Operating Systems, Design and Implementation », 3rd ed, Pearson}
    \end {center}

    \begin {itemize}
	\item IBM 7094 : très onéreux
	\item IBM 1401 : peu coûteux (pour l'époque) \\
	    \implique dédié aux
	    transferts entre périphériques lents et bandes magnétiques
	    (rapides)
	\item transfert manuel des bandes \\
	    \implique évolution vers une connexion directe \\
	    \implique évolution vers des disques durs

    \end {itemize}
\end {frame}

\begin {frame} {Historique -- Spooling}
    En résumé :

    \begin {itemize}
	\item évolution vers des « périphériques » plus
	    « intelligents »
	\item fonctionnent en parallèle avec le processeur
	    central

    \end {itemize}

    \medskip

    \implique il faut maintenant tirer parti de ce parallélisme

    \begin {itemize}
	\item le moniteur doit gérer les ressources matérielles lentes
	    pour en exploiter le parallélisme
    \end {itemize}
\end {frame}

\begin {frame} {Historique -- Multiprogrammation}
    Et lorsque le programme attend le résultat d'une entrée/sortie ?

    \begin {itemize}
	\item « démocratisation » des périphériques
	\item ex: stockage ou récupération d'une donnée temporaire
	    sur un périphérique (bande magnétique, disque dur, etc.)

	\item temps mort pour le processeur \\
	    \implique utiliser ce temps mort pour un autre programme

    \end {itemize}

    \medskip

    Introduction de la multiprogrammation \implique \textbf {superviseur}
\end {frame}

\begin {frame} {Historique -- Multiprogrammation}

    Le superviseur charge plusieurs programmes en mémoire~:
    \begin {itemize}

	\item lorsque le programme en cours demande une E/S, le
	    superviseur démarre le programme suivant
	\item lorsque le contrôleur d’E/S signale la fin de l’E/S,
	    le premier programme reprend son exécution
    \end {itemize}

    \medskip

    Exemple~: Atlas Supervisor de l'U. de Manchester (1962)
\end {frame}

\begin {frame} {Historique -- Multiprogrammation}
    Questions posées par la multiprogrammation~:

    \begin {itemize}
	\item comment protéger les programmes les uns des autres ?
	    \\
	    \implique accès interdit aux variables d'un autre programme

	    \implique \textbf {unité de gestion mémoire} (composant matériel)

	\item comment le superviseur reprend le contrôle
	    lorsque le contrôleur d'E/S a terminé ?

	    \implique \textbf {mécanisme d'interruptions}
    \end {itemize}

\end {frame}

\begin {frame} {Historique -- Télétype}
    \begin {minipage} [c] {.4\linewidth}
	\figcredit{term-tty}{.9}{Teletype Model ASR33 -- {\ccbysa} AlisonW \href{https://commons.wikimedia.org/wiki/File:Teletype_with_papertape_punch_and_reader.jpg}{Wikimedia Commons}}
    \end {minipage}%
    \begin {minipage} [c] {.6\linewidth}
	Ceci est une révolution !
	\begin {itemize}
	    \item changement fondamental dans l'interaction
		avec l'ordinateur
	    \item plus besoin d'attendre le passage du bac de
		cartes perforées sur l'ordinateur
	    \item commandes tapées au clavier \\
		\implique langage de commandes
	    \item résultat directement imprimé
	\end {itemize}
    \end {minipage}

\end {frame}

\begin {frame} {Historique -- Télétype}

    \begin {itemize}
	\item c'est un périphérique comme un autre...

	\item ... mais l'interaction modifie le besoin

	\item le superviseur lance des actions suite à des
	    commandes tapées à la console

	\item usage « interactif » $\neq$ usage « batch »

    \end {itemize}

    \bigskip

    Télétype en action : \url {https://youtu.be/X9ctLFYSDfQ}

\end {frame}

\begin {frame} {Historique -- Temps partagé}
    Connecter plusieurs terminaux à un même ordinateur :

    \begin {itemize}
	\item accueillir plusieurs utilisateurs
	\item mieux rentabiliser le coût d'un ordinateur
	\item connexion via une liaison série directe, ou via un modem
	\item donner à chacun l'impression d'avoir « son » ordinateur
	\item systèmes à temps partagé
    \end {itemize}

    Exemples~:
    \begin {itemize}
	\item CTSS (Compatible Time-Sharing System) : MIT (1961)
	    \\
	    IBM 7094 modifié par IBM, 32 utilisateurs maximum
	\item STSS (Stanford Time-Sharing System) : Stanford (1963)
	    \\
	    DEC PDP-1, 12 terminaux
    \end {itemize}
\end {frame}

\begin {frame} {Historique -- Temps partagé}
    Allouer le processeur pendant des petites portions de temps à
    chaque utilisateur~:

    \begin {itemize}
	\item quantum : durée fixée par le système (ex: 20 ms)
	\item pour un être humain, les programmes semblent tourner
	    en parallèle
    \end {itemize}


    \begin {center}
	\fig {quantum} {.9}
    \end {center}

\end {frame}

\begin {frame} {Historique -- Temps partagé}
    Questions posées par le temps partagé~:

    \begin {itemize}
	\item comment reprendre le contrôle à la fin de quantum ?
	    \\
	    \implique mécanisme d'\textbf {horloge} avec interruption

	\item comment gérer plusieurs utilisateurs ?
	    \\
	    \implique identité : validation et autorisations \\
	    \implique mécanismes logiciels

	\item comment éviter qu'un utilisateur outrepasse ses droits ?
	    \\
	    \implique \textbf {modes d'exécution} privilégié et non
		privilégié \\
	    \implique quelques instructions interdites en mode non
		privilégié

	    \smallskip

	    Exemple~:
	    \begin {itemize}
		\item accès au disque : réservé au mode
		    privilégié
		\item programmes utilisateurs : exécutés
		    en mode non privilégié
		    \\
		    \implique passage par le système pour
		    accéder aux fichiers
	    \end {itemize}

    \end {itemize}

\end {frame}

\begin {frame} {Historique -- Appel système}
    Lorsqu'un programme souhaite effectuer (par exemple) un accès
    disque, il fait un \textbf {appel système} :

    \begin {itemize}
	\item instruction spéciale (TRAP, SVC, INT, etc. suivant le
	    processeur)

	\item provoque (entre autres) :
	    \begin {itemize}
		\item basculement en mode privilégié
		\item déroutement du programme vers une adresse spécifique
	    \end {itemize}

	\item adresse spécifique = système d'exploitation
	\item \textbf {vérification} des paramètres, des droits, etc.
	    \\
	    \implique pour éviter qu'un utilisateur outrepasse ses droits
	\item réalisation de l'action demandée par l'appel système

    \end {itemize}

\end {frame}

\begin {frame} {Historique -- Temps partagé}

    Début des années 1970 : terminaux à écran cathodique

    \begin {center}
	\figcredit{term-adm3a}{.4}{ADM 3A {\ccbysa} Rama \href{https://commons.wikimedia.org/wiki/File:Lear_Siegler_ADM-3A-IMG_1508.jpg}{Wikimedia Commons}}
    \end {center}

    \medskip

    Âge d'or des « mainframes »

    \implique stabilisation des systèmes d'exploitation autour des
    concepts vus précédemment

\end {frame}

\begin {frame} {Bilan}
    Fonctionnalités offertes par un système d'exploitation

    \begin {itemize}
	\item rentabiliser l'usage de l'ordinateur
	    \\
	    \implique utiliser tous les temps morts
	\item partager un ordinateur entre plusieurs utilisateurs
	    \\
	    \implique partager équitablement les ressources matérielles

	\item faciliter l'accès aux périphériques
	    \\
	    \implique offrir une interface avec le matériel
	    \\
	    \implique pour tirer parti du parallélisme des périphériques

	\item permettre à des utilisateurs d'éditer, développer,
	    lancer des programmes
    \end {itemize}

    ... le tout en offrant les garanties de sécurité nécessaires
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Le noyau
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Le noyau}

\begin {frame} {Continuons l'historique}

    Systèmes d'exploitation marquants~:

    \begin {itemize}
	\item Burroughs MCP (1961)
	    \begin {itemize}
		\item conçu pour l'ordinateur Burroughs B5000
		\item premier SE programmé en \textbf {langage de
		    haut niveau}
		\item beaucoup d'éléments innovants (multiprocesseur,
		    mémoire virtuelle)
	    \end {itemize}
	\item IBM OS/360 (1964)
	    \begin {itemize}
		\item SE « universel » pour les ordinateurs IBM System/360
		\item énorme (des dizaines de millions de lignes d'assembleur)
		\item \textbf {complexe}, beaucoup de retards
		\item F. Brooks « The Mythical Man-Month »
	    \end {itemize}
	\item CTSS (\textbf {MIT}, 1961)
	    \begin {itemize}
		\item premier système multi-utilisateur
		\item IBM 7094 modifié (2 exemplaires)
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Multics}

    \begin {itemize}
	\item Multics (MULtiplexed Information and Computing Service)
	    \ctableau {\fD} {|ll|} {
		1963 & spécification (MIT) \\
		1964 & démarrage du projet \\
		1964 & General Electric + Bell Labs rejoignent le MIT \\
		1968 & première version « self-hosting » \\
		1969 & \textbf {Bell Labs} se retirent du projet \\
		1973 & annonce commerciale \\
		1985 & fin du développement \\
		2000 & fermeture du dernier site (Min Défense Canada) \\
	    }

	    \begin {itemize}
		\item Système très ambitieux
		\begin {itemize}
		    \item écrit en langage de haut niveau (PL/1)
		    \item accès uniforme à la mémoire et aux fichiers
		    \item système de fichiers hiérarchique
		    \item édition de liens dynamique
		    \item reconfiguration matérielle dynamique
		    \item processus « démons »
		    \item sécurité (certification B2 en 1985)
		\end {itemize}
		\item 84 sites au total (dont 31 universités/centres français)
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Unix}
    \begin {minipage} [c] {.7\linewidth}
    \begin {center}
	\figcredit{pdp11}{}{Ken Thompson et Dennis Ritchie devant un PDP-11 \\ {\ccbysa} Peter Hamer \href{https://www.bell-labs.com/usr/dmr/www/picture.html}{Bell Labs} et \href{https://commons.wikimedia.org/wiki/File:Ken_Thompson_(sitting)_and_Dennis_Ritchie_at_PDP-11_(2876612463).jpg}{Wikimedia Commons}}
    \end {center}
    \end {minipage}%
    \begin {minipage} [c] {.3\linewidth}
    \begin {itemize}
	\fC
	\item Dennis Ritchie \\ (debout)
	\item Ken Thompson \\ (assis)
	\medskip
	\item DEC PDP-11 \\ (arrière-plan)
    \end {itemize}
    \end{minipage}
\end {frame}

\begin {frame} {Unix}

    \begin {itemize}
	\item Unix
	    \ctableau {\fD\rowcolors {2} {bleufonce} {bleuclair}} {|ll|} {
		\rca
		1969 & Bell Labs se retirent du projet Multics \\
		      & récupération d'un PDP-7, écriture en assembleur \\
		1971 & achat PDP-11/20 en échange d'un formateur de texte \\
		1972 & création du langage C et réécriture d'Unix en C \\
		1973 & premières diffusions \\
		1979 & Unix v7 (environ 10 000 lignes de C et un peu
		    d'assembleur) \\
		1980 & distribution 4BSD \\
		1992 & procès AT\&T vs BSDi vs Berkeley (fin en 1994) \\
	    }

	    \begin {itemize}
		\item Beaucoup d'idées novatrices
		    \begin {itemize}
			\item simple
			\item écrit en langage C
			\item séparation nette \textbf {noyau} / utilitaires
			    (ou applications)
			\item tout est fichier
			\item pas de structure interne des fichiers
			\item interface utilisateur simple (minimaliste)
			\item philosophie Unix (kiss : « keep it simple, stupid »)
			\item portable
		    \end {itemize}
	    \end {itemize}
	    
    \end {itemize}

\end {frame}

\begin {frame} {Le noyau}
    Philosophie Unix \implique approche minimaliste,
    à l'opposé des systèmes d'exploitation précédents

    \medskip

    Le noyau a deux objectifs fondamentaux~:
    \begin {itemize}
	\item \textbf {partager équitablement les ressources}
	\item \textbf {garantir la sécurité des données}
    \end {itemize}

\end {frame}

\begin {frame} {Le noyau}
    Partager équitablement les ressources~:

    \begin {itemize}
	\item mémoire vive
	\item temps processeur
	\item carte réseau
	\item espace disque
	\item accès aux disques
	\item etc.
    \end {itemize}
\end {frame}

\begin {frame} {Le noyau}
    Garantir la sécurité des données

    \begin {itemize}
	\item un processus ne doit pas accéder aux données d'un autre
	    processus (sauf si autorisé)

	\item un utilisateur ne doit pas accéder à un fichier non autorisé

	\item un utilisateur ne doit pas terminer un processus d'un
	    autre utilisateur

	\item etc.

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau}

    Pour atteindre les deux objectifs fondamentaux :

    \begin {itemize}
	\item le \textbf {noyau} s'exécute en \textbf {mode privilégié}
	    \\
	    \implique il a accès à toutes les ressources matérielles

	\item le \textbf {noyau} fournit des \textbf {abstractions}
	    \\
	    \implique pas d'accès incontrôlé aux ressources matérielles

	\item les programmes (applications) s'exécutent, dans le
	    contexte de \textbf {processus}, en \textbf {mode non
	    privilégié}
	    \\
	    \implique appels au noyau pour exécuter certaines opérations

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau -- Abstractions [1/2]}

    Principales abstractions offertes par le noyau :

    \begin {itemize}
	\item \textbf {processus}
	    \begin {itemize}
		\item permettre à plusieurs programmes de co-exister
		    en mémoire et partager le temps processeur
	    \end {itemize}

	\item \textbf {fichier}
	    \begin {itemize}
		\item ne pas laisser les programmes accéder 
		    directement aux disques durs et lire des données
		    non autorisées
		\item \implique notion associée : droits d'accès
		\item \implique notion associée : répertoires pour mieux
		    ranger les fichiers
	    \end {itemize}

	\item \textbf {utilisateur}
	    \begin {itemize}
		\item permettre à plusieurs utilisateurs de se connecter
		    simultanément à l'ordinateur via des terminaux
		    différents (ou via le réseau)
		\item \implique notion associée : droits d'accès
		\item \implique notion associée : groupe
	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau -- Abstractions [2/2]}

    \begin {itemize}
	\item \textbf {périphérique}
	    \begin {itemize}
		\item accès contrôlé aux périphériques (terminal,
		    imprimante, souris, écran tactile du téléphone,
		    GPS, accès téléphonique mobile, etc.)
		\item notion associée : droits d'accès
		\item notion associée : exclusion mutuelle

	    \end {itemize}

	\item \textbf {tube}
	    \begin {itemize}
		\item canal de communication entre deux (ou plus)
		    processus sur le même système

	    \end {itemize}

	\item \textbf {socket}
	    \begin {itemize}
		\item canal de communication entre deux (ou plus)
		    processus sur des machines distinctes

	    \end {itemize}

	\item \textbf {signal}
	    \begin {itemize}
		\item prise en compte d'événements asynchrones
		\item exemples : \texttt{ctrl-c}, expiration d'une alarme,
		    déconnexion, etc.

	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau -- Primitives système}

    Les appels au noyau sont les \textbf {primitives système}

    \begin {itemize}
	\item Interface de programmation du noyau
	    \\
	    \textit {Application Programming Interface} (API)
	\item Forme : appels de fonction en langage C
	\item Exemples :
	    \begin {itemize}
		\item 
		    \code{int open (const char *path, int flag, mode\_t mode)}
		\item
		    \code{uid\_t getuid (void)}
	    \end {itemize}
    \end {itemize}


\end {frame}

\begin {frame} {Le noyau -- Primitives système}
    \begin {center}
	\fig {noyau} {.7}
    \end {center}
\end {frame}

\begin {frame} {Le noyau -- Primitives système}
    Définition :

    \begin {quote}
	Le noyau est constitué de l'ensemble minimum des primitives
	système nécessaires pour atteindre les deux objectifs
	fondamentaux

    \end {quote}

\end {frame}

\begin {frame} {Le noyau}
    \begin {itemize}
	\item Démarrage de l'ordinateur : noyau installé
	    en mémoire \\
	    \implique il y restera jusqu'à la fin (redémarrage,
	    arrêt, ou crash)

	\item Le noyau s'exécute en mode privilégié

	    \begin {itemize}
		\item code sensible
		\item difficile à développer, à mettre au point
		\item plus il est petit, mieux c'est
		\item tout ce qui peut être mis ailleurs doit l'être
		    \\
		    \vspace* {0.8mm}
		    Exemple :
		    \begin {itemize}
			\item pour le noyau, un utilisateur = un nombre
			    entier
			\item \code{ls -l} affiche des noms de login
			\item \implique la conversion
			    « entier $\leftrightarrow$ nom » est
			    effectuée par \code{ls}

		    \end {itemize}
	    \end {itemize}

	\item Mise en évidence du concept de noyau \\
	    \implique apport majeur d'Unix

    \end {itemize}

\end {frame}

\begin {frame} {Le noyau -- Primitives système}

    Toutes les applications utilisent l'API des primitives système...

    \begin {itemize}
	\item le plus souvent via des fonctions de bibliothèque qui
	    utilisent elles-mêmes des primitives système

	\item exemples :
	    \begin {itemize}
		\item fonctions de plus haut niveau, conçues pour être
		    plus faciles à utiliser (ex : \code{printf}
		    vs \code{write})

		\item bibliothèque de fonctions mathématiques

		\item interface graphique : fenêtres, souris, etc.
	    \end {itemize}
    \end {itemize}

\end {frame}

\begin {frame} {Le noyau -- Primitives système}
    ... et même les programmes en Python utilisent l'API des primitives
    système :

    \begin {itemize}
	\item \lst{open.py} {\fD} {}
	\item \code{open} en Python correspond à une portion du
	    programme «~interprète Python~» qui appelle la primitive
	    système \code{open}
	\item \code{print} en Python correspond à une portion du
	    programme «~interprète Python~» qui appelle la fonction
	    de bibliothèque \code{printf} qui appelle elle-même la
	    primitive système \code{write}

    \end {itemize}
\end {frame}

\begin {frame} {Le noyau -- Primitives système}
    \begin {casseroleblock} {Commande strace}
    Commande \code{strace} (sur Linux)~: affiche les primitives
    système appelées par un programme~:

    \lst{strace.output} {\fE} {}

    \medskip

    \implique permet de suivre à la trace toutes les actions d'un
    processus

    \end {casseroleblock}
\end {frame}

\begin {frame} {Interface utilisateur}
    \ctableau {\fD} {|ccc|} {
	\textbf{Date} & \textbf{Stations de travail}
		& \textbf{Ordinateurs personnels}
	    \\ \hline
	1968 & \multicolumn {2} {c|} {Prototype de souris par
		Douglas Englebart (Stanford Research Institute)} \\
	1973 & Alto (Xerox) & \\
	1981 & Display Manager (Apollo Computer) & \\
	1983 & W Window System (Stanford) & Lisa (Apple) \\
	1984 & X Window System (MIT) & \\
	1985 & HP Windows (HP),	& Amiga Workbench (Commodore), \\
	\rca     & Sunview (Sun)	& Windows (Microsoft), etc. \\
    }

    La gestion d'une interface utilisateur « graphique » n'est pas
    du ressort du noyau :
    \begin {itemize}
	\item Souris, écran graphique, écran tactile (tablette, etc.) \\
	    \implique périphériques gérés par le noyau
	\item Système de fenêtrage \\
	    \implique applications
    \end {itemize}
\end {frame}

\begin {frame} {Qu'est-ce qu'un système d'exploitation ?}
    Retour sur notre interrogation du début :
    \begin {itemize}
	\item Unix (l'original, puis les *BSD) : noyau + applications
	\item Linux est un noyau
	    \begin {itemize}
		\item des organisations (bénévoles ou commerciales) ont
		    réuni des applications, les ont compilées et ont
		    diffusé, avec le noyau, des \textbf {distributions}
		    prêtes à l'emploi

	    \end {itemize}
	\item Windows : noyau + applications
	\item Android, iOS : noyau (avec support de périphériques tactiles,
	    audio, GSM, GPS, etc.) + applications
	\item box Internet, montre connectée, voiture, etc. : noyau
	    + applications spécifiques
	\item etc.
    \end {itemize}
\end {frame}

\begin {frame} {Quelques systèmes spécialisés}
    Spécialisation de systèmes d'exploitation :

    \begin {itemize}
	\item systèmes temps réel

	    \begin {itemize}
		\item borner le temps de réaction à un événement externe
		    \begin {itemize}
			\item si le pilote tire sur le manche, il faut
			    transmettre la commande à la gouverne en
			    moins de $x$ ms

		    \end {itemize}
	    \end {itemize}

	\item systèmes contraints (ex: Internet des Objets)

	    \begin {itemize}
		\item minimiser l'empreinte mémoire
		\item minimiser la consommation énergétique
	    \end {itemize}

    \end {itemize}
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% POSIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {POSIX}

\begin {frame} {Interface des primitives système}
    Primitives système = ensemble de fonctions (appelables en C)
    pour accéder aux services offerts par le noyau

    \begin {itemize}
	\item À l'origine (Unix v6, 1975) : 43 primitives
	\item Évolutions ultérieures (années 1980) :
	    \begin {itemize}
		\item Bell Labs, AT\&T
		\item U. de Berkeley
		\item Essor commercial
	    \end {itemize}
	\item Résultat
	    \begin {itemize}
		\item de nombreuses divergences \implique incompatibilités
		\item les programmes ne sont plus portables
		\item les utilisateurs ne sont pas contents
	    \end {itemize}
	\item Il faut normaliser l'existant \implique POSIX
    \end {itemize}

\end {frame}

\begin {frame} {POSIX}

    Comité POSIX de l’IEEE
    \begin {itemize}
	\item IEEE = Institute of Electrical and Electronics Engineers
	    \\
	    (association américaine, rédige des textes normatifs)
	\item POSIX = Portable Operating System Interface
	\item Norme IEEE : IEEE Std 1003.*
	\item Norme internationale : ISO/IEC 9945
	\item Première version : 1988
	\item Version actuelle : 2017
    \end {itemize}

    \medskip

    \url {https://pubs.opengroup.org/onlinepubs/9699919799/}

    \bigskip

    Impact majeur \implique aucun système d'exploitation ne peut
    se permettre de ne pas être compatible POSIX
\end {frame}

\begin {frame} {POSIX}

    POSIX normalise beaucoup de choses :

    \begin {itemize}
	\item des primitives système et des fonctions de bibliothèque
	\item des commandes (\code{sh}, \code{ls}, \code{tr}, etc.)
	\item des extensions (temps réel, threads, sémaphores, etc.)
    \end {itemize}

    \medskip

    POSIX ne normalise pas tout :
    \begin {itemize}
	\item une implémentation peut avoir ses propres extensions \\
	    \implique non normalisées \implique non portables
	\item Exemple avec \code{ls} :
	    \begin {itemize}
		\item POSIX 2013 : 23 options (c'est beaucoup...) \\
		\item GNU (Linux) : 58 options (c'est trop !) \\
	    \end {itemize}
	\item programmer portable \implique respecter POSIX
	    \begin {itemize}
		\item maxime des années 1980-90 :
		    \textit{The World is not a VAX}
		\item aujourd'hui, ce serait : \textit{The World is not Linux}
	    \end {itemize}

    \end {itemize}

\end {frame}
