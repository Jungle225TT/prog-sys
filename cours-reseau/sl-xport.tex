\def\inc{inc-xport}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Couche transport
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreA {Couche transport}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Intro
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {TCP/UDP - Présentation}

    IP est un protocole qui achemine les datagrammes~:
    
    \begin {itemize}
	\item d'une machine à une autre
	\item le mieux possible (\textit {best effort\/})
    \end {itemize}

    \implique besoin d'une couche \emph{transport} au dessus de IP.

\end {frame}


\begin {frame} {TCP/UDP - Présentation}

    TCP (\emph{Transmission Control Protocol\/}) est un protocole basé
    sur IP (encapsulé dans des datagrammes IP) qui~:

    \begin {itemize}
	\item assure une \emph{connexion}

	\item utilise la notion de port (16 bits) pour faire communiquer
	    deux applications sur des machines

	\item contrôle les erreurs pour assurer une transmission fiable

    \end {itemize}

\end {frame}


\begin {frame} {TCP/UDP - Présentation}

    UDP (\emph{User Datagram Protocol\/}) est également un protocole basé
    sur IP qui~:

    \begin {itemize}
	\item fonctionne en mode \emph{non} connecté

	\item utilise la notion de port (16 bits) pour faire communiquer
	    deux applications sur des machines

	\item n'assure pas la fiabilité de la transmission
 
    \end {itemize}

    \implique UDP = simplicité de IP accessible par les applications.

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% UDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {UDP}

\begin {frame} {UDP}

    Caractéristiques :

    \begin {itemize}
	\item transport non fiable
	\item sans connexion
	\item ordre des messages non respecté
    \end {itemize}

    \medskip

    Non fiable car :

    \begin {itemize}
	\item IP non fiable
	\item les messages sont bufferisés par le système
	    d'exploitation\\
	    \implique si l'application ou le système ne sont
	    pas assez rapides, des messages sont perdus

    \end {itemize}

\end {frame}


\begin {frame} {UDP}

    Exemples de ports UDP :

    \ctableau {} {|rcl|} {
	\textbf {Port} & \textbf {Nom} & \textbf {Signification} \\ \hline
	53 & domain & serveur de noms \\
	67 & bootps & serveur BOOTP/DHCP \\
	68 & bootpc & client BOOTP/DHCP \\
	69 & tftp & trivial file transfer \\
	123 & ntp & network time protocol \\
	161 & snmp & gestion de réseau \\
	162 & snmp-trap & alertes SNMP \\
	513 & who & rwho \\
    }
\end {frame}


\begin {frame} {UDP}

    \begin {center}
    \fig{udp-fmt}{.8}
    \end {center}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TCP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {TCP}

\begin {frame} {TCP}

    Caractéristiques :

    \begin {itemize}
	\item transport fiable
	\item circuit virtuel
	\item transferts bufferisés
	\item flux d'information non structuré
	\item full duplex
    \end {itemize}

    Une connexion TCP est identifiée par~:

    \begin {itemize}
	\item l'adresse IP et le port du programme source
	\item l'adresse IP et le port du programme destination
    \end {itemize}

\end {frame}


\begin {frame} {TCP}

    Exemples de ports TCP :

    \ctableau {} {|rcl|} {
	\textbf {Port} & \textbf {Nom} & \textbf {Signification} \\ \hline
	21 & ftp & transfert de fichiers \\
	22 & ssh & secure shell \\
	23 & telnet & connexion à distance \\
	25 & smtp & courrier électronique \\
	53 & domain & serveur de noms \\
	79 & finger & finger \\
	143 & imap3 & lecture de mail \\
	515 & printer & impression déportée \\
	631 & ipp & internet printing protocol \\
	6000 & X11 & X Window Version 11 \\
    }
\end {frame}


\begin {frame} {TCP}

    \begin {center}
    \fig{tcp-fmt}{.8}
    \end {center}
\end {frame}


\begin {frame} {TCP}

    Fiabilité~: lorsque A envoie un segment à B, il suffit que A
    attende l'accusé de réception avant d'envoyer le segment suivant \\

    \implique numéros de séquence et d'acquittement~:

    \bigskip

    \begin {minipage} {.65\linewidth}
	\begin {itemize}
	    \item valeur initiale aléatoire
	    \item numéro de séquence~= offset (relatif à la valeur initiale)
		du premier octet du segment
		dans le flux de données
	    \item numéro d'acquittement~= numéro de séquence du prochain
		segment attendu
	\end {itemize}
    \end {minipage}
    \hfill
    \begin {minipage} {.25\linewidth}
	\fig{tcp-seq}{}
    \end {minipage}

\end {frame}


\begin {frame} {TCP}

    Problème~: comment échanger les numéros ? \\
    \implique établissement de la connexion

    \bigskip

    \begin {minipage} {.65\linewidth}
	Ouverture de connexion :

	\begin {itemize}
	    \item ouverture \emph{passive} :  accepter et attendre les
		connexions (serveur)

	    \item ouverture \emph{active} : contacter l'application
		distante et s'y connecter (client)

	\end {itemize}
    \end {minipage}
    \hfill
    \begin {minipage} {.30\linewidth}
	\fig{tcp-con}{}
    \end {minipage}

    \medskip
    Utilisation des flags SYN et ACK pour ouvrir la connexion et échanger
    les numéros de séquence respectifs

\end {frame}


\begin {frame} {TCP}

    \begin {minipage} {.65\linewidth}
	Fermeture de la connexion~:
	\begin {itemize}
	    \item nécessite les fermetures des deux
		parties
	    \item la partie n'ayant pas fermé la
		connexion peut continuer à envoyer des données

	\end {itemize}
    \end {minipage}
    \hfill
    \begin {minipage} {.30\linewidth}
	\fig{tcp-fin}{}
    \end {minipage}
\end {frame}


\begin {frame} {TCP -- Diagramme d'états}

    \begin  {center}
	\fig{tcp-auto}{.6}
    \end  {center}

    Note~: \emph{a}/\emph{b} signifie
	«~réception de \emph{a}, émission de \emph{b}~»
\end {frame}


\begin {frame} {TCP}

    Format des options de l'en-tête TCP~:
    \begin {center}
    \fig{tcp-optfmt}{.7}
    \end {center}

    Exemples d'options~:
    \begin {center}
    \fig{tcp-optex}{.7}
    \end {center}

\end {frame}

\begin {frame} {TCP}

    Option MSS = \textit {Maximum Segment Size}

    \bigskip

    Quelle valeur choisir~?
    \begin {itemize}
	\item doit être adapté aux buffers de réception et
	    d'émission

	\item ne doit pas être trop petit \\
	    \implique minimiser l'overhead

	\item ne doit pas être trop grand \\
	    \implique perte d'un fragment nécessite la retransmission
	    de tout le datagramme

    \end {itemize}

    Initialisation~:
    \begin {itemize}
	\item annonce par chaque partie à l'ouverture de connexion
	\item initialisé par défaut à la valeur du Path-MTU
    \end {itemize}
\end {frame}

\begin {frame} {TCP}

    Optimisations de TCP pour les grands réseaux~:

    \begin {itemize}
	\item fenêtres mobiles (\emph{sliding windows\/})
	\item algorithme de Nagle
	\item acquittements retardés (\emph{delayed acks\/})
	\item démarrage lent (\emph{slow start\/})
	\item évitement de congestion (\emph{congestion avoidance\/})
	\item retransmission rapide (\emph{fast retransmit\/})
	% \item remise en route rapide (\emph{fast recovery})
    \end {itemize}
\end {frame}

\begin {frame} {TCP}

    Fiabilité : lorsque A envoie un segment à B, il suffit que A attende
    l'accusé de réception avant d'envoyer le segment suivant \\
    \implique algorithme de type \emph{stop-and-wait}

    \bigskip

    Problème : mécanisme trop lent (entre 100 et 500 ms pour traverser
    l'Atlantique) car synchronisation à chaque segment

    \bigskip

    Solution : \emph{Sliding Windows} (fenêtre mobile)
\end {frame}

\begin {frame} {TCP}

    Principe avec une fenêtre mobile de taille 3000 octets~:

    \medskip

    \begin {minipage} {.65\linewidth}
	\begin {enumerate}
	    \item l'émetteur envoie les 3000 premiers octets (3 segments
		de taille 1000)

	    \item quand l'émetteur reçoit l'acquittement des 1000
		premiers octets (premier segment), il peut envoyer
		1000 octets supplémentaires (quatrième segment)

	\end {enumerate}
    \end {minipage}
    \hfill
    \begin {minipage} {.30\linewidth}
	\fig{tcp-win}{}
    \end {minipage}
\end {frame}


\begin {frame} {TCP}

    Comment déterminer la taille des fenêtres ?

    Principes de la taille de fenêtre :

    \begin {itemize}
	\item mesurée en octets
	\item place disponible dans la mémoire
	    du récepteur
	\item c'est au récepteur d'annoncer la taille de la fenêtre
	    disponible
	\item champ « Window Size » de l'en-tête TCP

    \end {itemize}

    Attention : ne pas confondre numéro d'acquittement et taille de
    fenêtre
\end {frame}


\begin {frame} {TCP}

    \begin {minipage} {.45\linewidth}
    Exemple~:
    \begin {itemize}
	\item taille de fenêtre initiale = 1000 octets
	\item taille maximum de segment = 500 octets
    \end {itemize}
    \end {minipage}
    \hfill
    \begin {minipage} {.5\linewidth}
    \fig{tcp-winack}{}
    \end {minipage}

\end {frame}


\begin {frame} {TCP}

    Problème~: taille de fenêtre sur 16 bits \\
    \implique limite = 65535 octets \\
    \implique bien peu pour des connexions à plusieurs Gb/s !

    \bigskip

    Option \emph{Window Scale Option} lors de la connexion TCP~: \\
    si une des parties donne une valeur \emph{n}, toutes ses
    annonces de taille de fenêtre seront à multiplier par 2$^n$

\end {frame}

\begin {frame} {TCP -- Mesure du RTT}
    Hauts débits + délais importants \implique numéros de séquence
    rapidement épuisés
    \\
    \implique comment savoir si un segment est déjà reçu ou non ?

    \medskip

    Exemple~: avec un RTT de 500 ms et un débit de 1 Gb/s, les $2^{32}$
    numéros possibles sont épuisés en environ 2 secondes

    \medskip

    \implique mesurer le RTT moyen sur un lien \\
    \implique calculer le RTO (\emph {retransmission timeout\/}) minimal

\end {frame}

\begin {frame} {TCP -- Mesure du RTT}
    Option «~Timestamp~» pour mesurer le RTT sur une machine

    \medskip

    \begin {minipage} {.63\linewidth}
	\begin {itemize}
	    \item l'émetteur place une estampille temporelle dans TSval
	    \item le récepteur renvoie TSval dans TSecr (\emph {echo
		reply\/})
	    \item l'émetteur calcule le RTT
	    \item en cas d'acquittement groupé, le récepteur renvoie
		l'estampille du dernier message arrivé

	\end {itemize}
    \end {minipage}
    \begin {minipage} {.25\linewidth}
	\fig{tcp-ts}{1.4}
    \end {minipage}

    \medskip

    (en pratique, les $t_i$ dans l'option Timestamp ne sont pas
    directement l'horloge de l'émetteur, pour ne pas donner d'indication
    à un éventuel pirate \implique facteur multiplicatif secret par
    connexion)

\end {frame}


\begin {frame} {TCP -- Algorithme de Nagle}

    Problème~: certains protocoles comme TELNET envoient
    un caractère par segment (\emph{tinygrams\/}) \\
    \implique gâchis de ressources réseau \\
    \implique congestion sur des réseaux lents

    \medskip

    Solution~:

    \begin {itemize}
	\item pas plus d'un «~petit segment~» non acquitté
	\item si on a d'autres petits segments à envoyer, on
	    attend l'acquittement du dernier petit segment \\
	    \implique \emph{bufferisation} automatique
	\item «~petit segment~» = plus petit que MSS (taille maximum
	    d'un segment)
    \end {itemize}
\end {frame}

\begin {frame} {TCP -- Algorithme de Nagle}
    \begin {center}
	\fig{tcp-nagle}{.6}
    \end {center}

    Bénéfices~:
    \begin {itemize}
	\item algorithme auto-adaptatif
	\item les courtes données n'encombrent pas le réseau
	\item les grandes données sont envoyées rapidement
    \end {itemize}

    Contre-exemples~: X-Window (déplacement de souris) \\
    \implique désactivation~: \texttt {setsockopt(..TCP\_NODELAY..)}

\end {frame}



\begin {frame} {TCP}

    Problème~: les aquittements encombrent le réseau

    Solutions~:

    \begin {itemize}
	\item \emph{piggyback}~: inclure les acquittements dans des
	    segments contenant des données

	\item \emph{delayed ack}~: attendre (200 à 500~ms, ou le deuxième
	    segment de données par exemple) et
	    ne renvoyer éventuellement qu'un seul acquittement au lieu
	    de plusieurs \\
	    \implique éventuellement avec un segment de données

    \end {itemize}

\end {frame}


\begin {frame} {TCP}

    Problème~: si l'émetteur émet à vitesse maximum \\
    \implique saturation des liaisons \\
    \implique bufferisation par les routeurs intermédiaires \\
    \implique écroulement du réseau

    \medskip

    Solution~:

    \begin {itemize}
	\item l'émetteur conserve pour chaque connexion~:
	    \begin {itemize}
		\item une «~fenêtre de congestion~»
		\item une copie de la fenêtre de réception annoncée par
		    le récepteur
	    \end {itemize}
	\item l'émetteur n'envoie que le minimum des deux
	    fenêtres
	\item la fenêtre de congestion sert à ajuster le flux de
	    l'émetteur en fonction des congestions observées dans le
	    réseau.
    \end {itemize}

\end {frame}


\begin {frame} {TCP}

    Traitement des congestions~:

    \begin {itemize}
	\item démarrer lentement (algorithme \emph{slow-start\/}) : \\
	    la fenêtre démarre avec une taille de 1 segment, elle
	    augmente de 1 à chaque retour d'acquittement \\

	\item réagir rapidement en cas de congestion~: \\
	    1 segment perdu \implique taille de fenêtre divisée par 2 \\
	    \implique chute du débit à la première congestion

	\item anticiper les congestions (\emph{congestion avoidance\/}) \\
	    au delà d'un seuil (la moitié de la fenêtre de
	    congestion avant la dernière congestion), l'augmentation de
	    débit ne se fait que de 1 lorsque tous les acquittements
	    d'une fenêtre sont reçus \\
	    \implique augmentation ralentie
    \end {itemize}

\end {frame}


\begin {frame} {TCP -- Fast retransmit}
    Idée : accélérer les retransmissions sans attendre l'expiration
    du timeout

    \begin {minipage} {.53\linewidth}
	Principe~: au bout de 3 acquittements identiques, retransmettre
	le segment demandé sans attendre l'expiration du timeout
    \end {minipage}
    \hfill
    \begin {minipage} {.35\linewidth}
    \fig{tcp-fast}{}
    \end {minipage}

    

\end {frame}


\begin {frame} {TCP}

    Problème : transporter des données \emph{urgentes} (signaux,
    interruptions, conditions d'erreur, etc.)

    \medskip

    Solution : \emph{out of band data}

    \medskip

    Implémentation :

    \begin {itemize}
	\item flag \textit {URG} : signale des données urgentes
	\item \textit {Urgent Pointer} : localise les données dans
	    le flux
    \end {itemize}

    L'utilisateur est prévenu par un mécanisme dépendant du système
    d'exploitation (ex : signaux sous Unix)

\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Usurpation d'identité
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Usurpation d'identité}

\begin {frame} {Usurpation d'identité}

    L'association entre une adresse IP et une adresse MAC n'est par
    nature pas sécurisée

    \medskip

    Il est possible de «~forger~» un datagramme IP avec une adresse
    IP source usurpée

    \medskip

    \implique attaques par injection de données malicieuses

\end {frame}


\begin {frame} {Usurpation d'adresse IP (\textit {IP Spoofing\/})}

    Certains protocoles utilisent l'adresse IP source pour autoriser
    une communication (faiblesse d'authentification)
    
    \medskip

    Exemples~: rlogin, NFS, RPC, services «~protégés~» par \textit
    {tcp-wrappers} ou équivalent, etc.

    \medskip

    Attaque~:

    \begin {itemize}
	\item forger l'adresse source d'un datagramme IP
	\item la réponse est envoyée à l'adresse usurpée 
	    \implique souvent utilisé avec «~vol de port~»
    \end {itemize}

    Prévention~:

    \begin {itemize}
	\item ne pas authentifier sur la simple adresse IP
	\item filtrer les adresses internes sur le port externe du
	    routeur ou du pare-feu
    \end {itemize}

\end {frame}


\begin {frame} {TCP -- Usurpation d'identité}

    Usurpation d'adresse IP avec TCP~: un pirate essaye d'envoyer
    des données à une victime en usurpant l'adresse IP d'un innocent.

    \begin {center}
	\fig{ip-spooftcp}{.5}
    \end {center}

    Difficultés~:

    \begin {itemize}
	\item deviner le numéro de séquence initial de la victime
	\item empêcher l'innocent de perturber la conversation
    \end {itemize}

    \implique attaque de type «~émission seulement~» \\
    (ex: mail toto@labas.fr < /etc/passwd)

\end {frame}


\begin {frame} {TCP -- Vol de session (\textit {session hijacking\/})}

    Une fois la connexion établie (y compris l'authentification
    éventuelle) entre le client et le serveur, le pirate usurpe
    l'adresse de l'innocent pour envoyer des données malicieuses
    à la victime

    \begin {center}
	\fig{tcp-hijack}{.5}
    \end {center}
\end {frame}


\begin {frame} {TCP -- Vol de session (suite)}

    Difficultés~:

    \begin {itemize}
	\item deviner le numéro de séquence de l'innocent
	\item empêcher l'innocent d'émettre des paquets (vol de
	    port ou duperie de résolution d'adresse), ou gérer la
	    tempête de demandes de réémissions
    \end {itemize}

    \implique des logiciels rendent cette exploitation simple~:
    T-Sight (Windows), Hunt (Linux), Juggernaut (Linux)

\end {frame}


\begin {frame} {TCP -- Syn flooding}

    \begin {minipage} {.35\linewidth}
	\begin {center}
	    \fig{tcp-synflood}{}
	\end {center}
    \end {minipage}
    \hfill
    \begin {minipage} {.6\linewidth}
	Attaque de type Syn Flooding~:
	\begin {itemize}
	    \item sur réception d'un SYN, le serveur réserve un
		«~état~»
	    \item le pirate envoie des SYN avec des adresses IP
		usurpées
	    \item la victime répond avec des SYN+ACK aux adresses
		sources
	    \item au bout d'un moment, la table des connexions entrantes
		de la victime est saturée
	\end {itemize}

	\implique attaque de type «~déni de service~»
    \end {minipage}

\end {frame}


\begin {frame} {TCP -- Syn flooding}

    Solution~: supprimer l'état dans la mémoire du serveur TCP \\
    \implique l'état est dans le numéro d'acquittement initial

    \begin {center}
	\fig{tcp-syncookie}{.60}
    \end {center}

\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filtrage/NAT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Filtrage/NAT}

\begin {frame} {Filtrage au niveau IP}

    Filtrage simple (par exemple sur un routeur) à l'aide d'un ensemble
    de règles~:

    \ctableau {\fC} {|cccc|} {
	\rcb
	\textbf{Interface} & \textbf{Adresse IP} & \textbf{Adresse IP} & \textbf{Action} \\
	\textbf{d'entrée} & \textbf{source} & \textbf{destination} & \\ \hline
	re0 & 198.18.0.0/15 & 198.51.100.0/24 & allow \\
	re0 & 0.0.0.0/0 & 198.51.100.1/32 & allow \\
	re0 & 0.0.0.0/0 & 198.51.100.0/24 & deny \\
    }
\end {frame}

\begin {frame} {Filtrage au niveau IP}
    Plusieurs approches possibles en fonction de l'implémentation :

    \begin {itemize}
	\item les règles sont examinées dans l'ordre
	    avec arrêt sur le premier (ou le dernier) cas trouvé
	\item ou alors la spécification la plus précise est
	    seule examinée
	\item action par défaut : « allow » ou « deny »
	\item interface = interface d'entrée ou de sortie
	\item certaines implémentations permettent d'inclure des
	    éléments complémentaires du datagramme IP (flags, type
	    de service, protocole, options)
    \end {itemize}

    Dans l'exemple précédent~: arrêt sur la première règle qui
    correspond aux deux adresses.

\end {frame}

\begin {frame} {Filtrage au niveau IP}
    Exemple (syntaxe IOS Cisco)~:

    \lst{acl.sh}{\fC}{}
\end {frame}

\begin {frame} {Filtrage au niveau TCP/UDP}

    Ajout de triplets <protocole, port source, port destination>~:

    \ctableau {\fD}  {|ccccccc|} {
	\rcb
	\textbf{Interface} & \textbf{Adresse IP} & \textbf{Port}
		& \textbf{Adresse IP} & \textbf{Port}
		& \textbf{Proto} & \textbf{Action} \\
	\textbf{d'entrée} & \textbf{source} & \textbf{src}
		& \textbf{destination} & \textbf{dst}
		& & \\ \hline
	eth0 & 198.18.0.0/15 & any & 198.51.100.0/24 & 22 & tcp & allow \\
	eth0 & 0.0.0.0/0 & any & 198.51.100.1/32 & 80 & tcp & allow \\
	eth0 & 0.0.0.0/0 & any & 198.51.100.0/24 & any & tcp & deny \\
	eth0 & 0.0.0.0/0 & any & 198.51.100.2/24 & 53 & udp & allow \\
	eth0 & 0.0.0.0/0 & any & 198.51.100.2/24 & any & udp & deny \\
	eth0 & 0.0.0.0/0 & - & 198.51.100.0/24 & - & icmp & deny \\
    }
\end {frame}

\begin {frame} {Filtrage au niveau TCP/UDP}

    Selon les implémentations~: prise en compte des flags TCP

    \medskip

    Exemple~: autoriser n'importe quelle connexion sortante SSH vers
    l'extérieur, et accepter les paquets en retour

    \begin {itemize}
	\item depuis le réseau interne vers l'extérieur~: accepter
	    tous les segments dont le port source = 22
	\item depuis l'extérieur vers le réseau interne~: n'accepter
	    que les segments TCP dont le port destination = 22 et les
	    flags TCP sont soit ACK, soit RST
    \end {itemize}

    Filtrage délicat à réaliser à grande échelle (i.e. beaucoup de
    protocoles, de réseaux et/ou d'interfaces)

\end {frame}

\begin {frame} {Filtrage au niveau TCP/UDP}

    Jusqu'ici : filtrage \textit {sans état} \\
    \implique filtrage réalisé avec les seules informations du paquet

    \medskip

    Mais :
    \begin {itemize}
	\item difficulté de créer des politiques de filtrage complexes
	\item vulnérabilité potentielle à des attaques avec des
	    datagrammes forgés
    \end {itemize}

    \medskip
    D'où le filtrage \textit {à états} :

    \begin {itemize}
	\item mémoriser un état pour des connexions TCP (voire des
	    échanges sans connexion comme UDP ou ICMP)

	    Exemple d'état pour TCP : \\
	    <IP S, port S, IP D, port D, statut, date dernier paquet>

	\item nécessité d'expirer les états \\
	    \implique envoi de messages \textit {keepalive} (application
	    ou pare-feu)

	\item plus complexe pour le routeur/pare-feu
    \end {itemize}

\end {frame}

\begin {frame} {Réseaux privés}

    Raréfaction des adresses IPv4
    \implique réseaux \textit {privés} (RFC 1918)

    \ctableau {} {|l|} {
	10.0.0.0/8 \\
	172.16.0.0/12 \\
	192.168.0.0/16 \\
    }

    Utilisation privée~:
    \begin {itemize}
	\item réseaux non connectés à Internet \\
	    exemples : robots d'une chaîne de montage, distributeurs
	    de billets, etc.
	\item réseaux non routés sur Internet
	\item réseaux à filtrer en bordure de réseau privé
	    (pour ne pas polluer l'Internet avec ces messages)
    \end {itemize}

    \implique de plus en plus utilisés avec de la traduction d'adresses

\end {frame}

\begin {frame} {Network Address Translation (NAT)}

    Traduction d'adresse IP sur un routeur/pare-feu, à l'aide d'une
    table de traduction « 1 pour 1 »~:

    \begin {center}
	\fig {nat-simple} {.9}
    \end {center}

    Rupture du paradigme « connectivité de bout en bout »~:

    \begin {itemize}
	\item protocoles supérieurs citant des adresses IP (ex: FTP)
	\item chiffrement IPSec (utilise les adresses IP)
	\item etc.
    \end {itemize}

\end {frame}

\begin {frame} {Network Address Translation (NAT)}
    \begin {itemize}
	\item NAT statique~: table de correspondance statique entre les
	    adresses internes et les adresses externes \\
	    \implique peu économe en adresses IP

	\item NAT dynamique~: la première connexion sortante réserve la
	    première adresse IP externe disponible \\
	    \implique peu utilisée en pratique

    \end {itemize}
\end {frame}

\begin {frame} {Network Address Translation (NAT)}
    Traduction <adresse, port> (NAPT) pour connexions sortantes~:

    \begin {center}
	\fig{nat-pat}{.9}
    \end {center}

    \begin {itemize}
	\item modèle quasi-universel pour le résidentiel et le GSM
	\item traduction possible en entrée (table statique de redirection)
	\item double-NAT, triple-NAT, carrier-grade NAT, etc.
    \end {itemize}

\end {frame}

\begin {frame} {Network Address Translation (NAT) -- Bilan}

    Avantages~:
    \begin {itemize}
	\item économie d'adresses IP
	\item refus des connexions entrantes \implique semblant de sécurité
    \end {itemize}

    \medskip
    Inconvénients~:
    \begin {itemize}
	\item faux sentiment de sécurité
	\item rupture du paradigme « connectivité de bout en bout »
	    \begin {itemize}
		\item protocoles supérieurs citant des adresses IP
		    ou des ports (H.323, FTP, DNS, etc.)
		\item protocoles où le serveur ouvre une nouvelle
		    connexion vers le client (ex: FTP, rlogin)
	    \end {itemize}
	\item ICMP
	\item implémentations défaillantes (fragmentation, extensions
	    «~récentes~» de protocoles, etc.)
	\item IPv6

    \end {itemize}
\end {frame}
