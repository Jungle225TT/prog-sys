void envoyer (datagramme_t datagramme) {
    IPdest = adresse_destination (datagramme) ;
    if (reseau (IPdest) == reseau (IPmoi)) {
	Eth = ARP (IPdest) ;
    } else {
	IProuteur = chercher_route (reseau (IPdest))
	if (non_trouve (IProuteur)) {
	    IProuteur = chercher_route (0.0.0.0) ;
	    if (non_trouve (IProuteur))
		erreur () ;
	}
	Eth = ARP (IProuteur) ;
    }
    encapsuler_trame (Eth, datagramme) ;
}
