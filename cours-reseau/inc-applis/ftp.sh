> ftp ftp.u-strasbg.fr			# lancement de la commande \texttt{ftp}
Connected to anubis.u-strasbg.fr.
220 ProFTPD 1.3.5rc3 Server (Osiris IPv6) [2001:660:2402::6]

Name (ftp.u-strasbg.fr:pda): anonymous	# connexion anonyme
331 Anonymous login ok, send your complete email address as your password

Password: pda@unistra.fr		# mot de passe = mon adresse électronique
230 Anonymous access granted, restrictions apply
Remote system type is UNIX.
Using binary mode to transfer files.

ftp> cd /pub/FreeBSD			# changement de répertoire sur le serveur
250 CWD command successful

ftp> get README.TXT			# récupération d'un fichier
local: README.TXT remote: README.TXT
200 EPRT command successful
150 Opening BINARY mode data connection for README.TXT (4259 bytes)
226 Transfer complete
4259 bytes received in 0.02 secs (174.6161 kB/s)

ftp> quit				# on s'en va
221 Goodbye.
