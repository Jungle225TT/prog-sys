volatile sig_atomic_t signal_recu = 0 ;

void f (int sig) {
  signal_recu = 1 ;
}

int main (...) {
  struct sigaction s ;

  s.sa_handler = f ;
  sigaction (SIGINT, &s, NULL) ;
  ...
  while (! signal_recu)
    ;			// boucle vide en attendant le signal
  // faire ce qui doit être fait lorsque le signal est pris en compte
}
