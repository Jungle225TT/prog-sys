int calcul (int a, int b, int c, int d) {
    return (a*b) + (c*d) ;
}

int somme_sauf (int x, int y, int sauf) {
    int s = 0 ;
    if (x != sauf)
	s = x+y ;
    else s = y ;
    return s*2 ;
}

int somme_pairs_sauf (int debut, int fin, int sauf) {
    int s = 0 ;
    for (int i = debut ; i <= fin ; i += 2)
	if (i != sauf)
	    s += i ;
    return s ;
}
