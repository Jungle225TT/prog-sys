% Pour vérifier qu'on écrit du LaTeX moderne
% \RequirePackage [orthodox] {nag}

\documentclass [twoside,openright,a4paper,11pt,french] {report}

    % Règles de typographie françaises
    \usepackage[francais]{babel}

    % Jeu de caractères UTF-8
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}

    \usepackage {color}

    % Inclure la bibliographie dans la table des matières comme une section
    \usepackage [numbib] {tocbibind}	% numbib : section numérotée

    % Fonte élégante
    \usepackage{mathpazo}
    \usepackage [scaled] {helvet}
    \usepackage{courier}

    % pour \EUR
    \usepackage{marvosym}

    % \usepackage{emptypage}

    % Utilisation de tableaux
    \usepackage{tabularx}

    % Utilisation d'url
    \usepackage{hyperref}
    \urlstyle{sf}

    % Utilisation d'images
    \usepackage{graphicx}
    \setkeys{Gin} {keepaspectratio}	% par défaut : conserver les proportions

    % Définition des marges
    \usepackage [margin=25mm, foot=15mm] {geometry}

    \parskip=2mm
    \parindent=0mm

    \pagestyle{plain}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page de garde
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\thispagestyle{empty}

\begin{center}
    % le ".1" est juste là pour montrer qu'on peut mettre des
    % dimensions non entières...
    \includegraphics [width=8.1cm] {logo-ufr.pdf}       

    \vfill

    {
	\large
	\textsc{
	    Master d'informatique \\
	    Parcours SIRIS \\
	    Science et Ingénierie des Réseaux, de l'Internet et des Systèmes
	}
    }

    \bigskip\bigskip
    \bigskip\bigskip

    {\huge Travail d'Étude et de Recherche}

    \bigskip\bigskip

    % Identité de l'auteur
    {\large Jacques \textsc{Sélère}}

    % Contact mail ou téléphone   
    {\small jacques.selere@etu.unistra.fr}

    \vfill

    % Titre du TER : mettez un titre utile
    {
	\huge
	\textsc{
	    Comment faire un bon \\
	    ~ \\
	    mémoire de TER ?
	}
    }

    \vfill
    \vfill

    \today

    \vfill

    {\large TER encadré par}

    \medskip

    % Identité de l'encadrant
    {\large Jean \textsc{Breille}}

    % Contact mail ou téléphone
    {\small jbreille@unistra.fr}

    \bigskip

    \bigskip

    % Logo de votre structure d'accueil
%    \includegraphics [height=2.5cm] {logo-entreprise.pdf}       

    % Supprimez le reste de cette page si vous vous servez de ce
    % fichier source comme modèle
    \vfill
    \begin{center}
	\tiny
	\copyright Pierre David,
	avec des contributions de 
	Stéphane Cateloin

	Disponible sur \url{https://gitlab.com/pdagog/ens}.

        Ce texte est placé sous licence « Creative Commons Attribution
	-- Pas d’Utilisation Commerciale 4.0 International » \\
	Pour accéder à une copie de cette licence,
	merci de vous rendre à l'adresse suivante
	\url{https://creativecommons.org/licenses/by-nc/4.0/}

	\includegraphics [scale=.5] {by-nc}
    \end{center}
    % Supprimez jusqu'ici


\end{center}

% Page blanche au dos de la page de garde
%\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table des matières
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

{
    \parskip=0pt
    \tableofcontents
}

% Page blanche entre la table des matières et le texte
\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Introduction}
    \label{chap:intro}

Le Travail d'Étude et de Recherche (TER) constitue une unité
d'enseignement importante de votre master d'informatique correspondant
à 6 crédits ECTS. Ceci signifie d'une part que l'obtention d'une
bonne note est cruciale pour votre réussite au master et d'autre part
que le TER nécessite un travail conséquent : celui-ci est estimé entre
150 et 180~heures de travail personnel, soit environ 12 à 15 heures
par semaine sur l'ensemble du semestre.

Le mémoire de TER constitue donc un élément essentiel de votre travail
: il doit permettre de mettre en valeur votre compréhension du sujet
et votre contribution, qui doit être originale et personnelle.

Votre mémoire sera lu par un «~rapporteur~», c'est-à-dire un
enseignant de l'équipe pédagogique (donc une personne ayant à priori
des compétences dans votre discipline) dont la mission est d'évaluer
votre compréhension du contexte et de la problématique ainsi que
votre contribution.

Ce document a pour but de rappeler les éléments attendus par le
rapporteur pour évaluer votre travail. Il est de votre intérêt de
bien lire ce document et fournir tous les éléments.

Enfin, le source\footnote{Voir le répertoire \texttt{mem-ter/} sur
\url{https://gitlab.com/pdagog/ens}} de ce document est destiné à vous
servir de modèle si vous souhaitez rédiger votre mémoire avec \LaTeX.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 2 : le fond
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Le fond}
    \label{chap:plan}

\section{Le travail attendu}

Le TER constitue une UE majeure du deuxième semestre du master.
Il est donc important de comprendre ce qui est attendu de vous.

\subsection {État de l'art}

Le TER débute généralement par un état de l'art autour du
sujet. Celui-ci peut concerner des algorithmes, mécanismes, protocoles,
technologies etc. qui ont été proposés dans la littérature.
Cette étape consiste donc avant tout en une recherche documentaire qui
doit aboutir à une bonne compréhension des travaux déjà menés dans
le domaine concerné. Paraphraser (ou pire, recopier) les documents ou
articles que vous lirez n'est pas utile ; ils doivent de toutes manière
figurer dans votre bibliographie.  L'exercice est plutôt d'en montrer
votre compréhension et votre maîtrise, de manière synthétique et
pédagogique. Une vision transversale, une comparaison des différentes
approches comportant par exemple des illustrations schématiques seront
appréciées.

\subsection {Bien identifier votre contribution personnelle}

À la suite de l'état de l'art, la ou les contributions personnelles
doivent être identifiées clairement. Vous devez pouvoir expliquer les
avancées que vous avez apportées au sujet traité. Pour cela, il faut
repositionner votre travail personnel par rapport à l'état de l'art,
en indiquant de quelle manière vous avez fait progresser la recherche
autour du sujet. Vous devez étayer votre argumentaire en présentant
des résultats quantitatifs et/ou qualitatifs.

\subsection {Bien présenter ses résultats}

Quels que soient les résultats que vous avez obtenus, il est impératif de
respecter un certain nombre de règles pour présenter votre travail afin qu'il
soit considéré comme une véritable démarche scientifique :

\begin{itemize}
    \item réplicabilité : vos expérimentations doivent pouvoir être
	répliquées dans un contexte équivalent. Il va de soi que la
	répétition de la même expérimentation doit aboutir à des
	résultats semblables. Cela implique que vous devez expliciter
	le contexte (outils utilisés, totalité des paramètres
	employés) ;

    \item analyse des résultats obtenus : vos résultats doivent être
        représentatifs (c'est-à-dire sans biais). Il est recommandé
        d'utiliser des outils statistiques (tests de corrélation,
        intervalles de confiance, etc.) et d'en fournir les conclusions ;

    \item représentation : les résultats numériques doivent souvent
	être représentés, en particulier s'ils regroupent un grand
	nombre d'échantillons. La représentation choisie est essentielle
	pour la compréhension de votre propos et l'interprétation
	qui en découlera.

    \item interprétation : tout résultat doit être interprété ;
	dire que ce que vous avez fait est meilleur ne suffit pas,
	il faut expliquer pourquoi.
    

    \item honnêteté intellectuelle : l'ensemble des étapes
	précédentes doit être mené avec une stricte honnêteté
	intellectuelle. Typiquement, il ne faut pas mener vos
	expérimentations uniquement dans des scénarii favorables,
	ignorer les biais d'échantillonnage, passer sous silence les
	résultats médiocres, les présenter de manière fallacieuse ou
	en faire une interprétation non objective, car cela s'apparente
	à de la fraude intellectuelle.

\end{itemize}

\section{Critères d'évaluation}

Votre mémoire sera évalué selon plusieurs critères (cf. grille en
annexe~\ref{anx:crit-eval}, page~\pageref{anx:crit-eval}), notamment~:

\begin{itemize}
    \item le contexte dans lequel s'inscrit le TER~: par exemple, si
	le sujet de votre TER est de trouver une nouvelle méthode de
	compression de foobar, il faut expliquer ce qu'est la compression
	de foobar, à quoi elle peut servir, et les différentes méthodes
	existantes et en quoi elles sont insuffisantes.

    \item le problème à résoudre : quel est le problème et en quoi la
	résolution de celui-ci est utile. Par exemple, ce que peut
	apporter une nouvelle méthode de compression de foobar et en
	quoi celle-ci peut sauver l'humanité, ou plus modestement à
	faire avancer la compression de foobaz qui elle-même est basée
	sur la compression de foobar.

    \item votre contribution personnelle~: il s'agit de la partie la
	plus importante de votre TER, celle sur laquelle vous avez passé
	le plus de temps. On attend de vous un travail original, étayé
	et argumenté. Il n'est pas question de résumer ou synthétiser
	un ou plusieurs articles, et encore moins de traduire des extraits
	d'un article, mais d'avoir une vraie valeur ajoutée dont la
	nature dépend du sujet : une implémentation, des évaluations,
	une reproduction d'expérience, un état de l'art d'un sujet, etc.

	Par le passé, nous avons constaté que des étudiants se sont
	contenté de traduire des fragments d'articles, ce qui ne
	constitue pas une contribution personnelle

    \item enfin, la mise en perspective de votre travail~: les avancées,
	ou à contrario les limites de votre travail, les pistes
	d'amélioration ou les utilisations de votre méthode pour
	rédoudre un autre problème majeur (par exemple l'utilisation
	de votre méthode pour améliorer la décompression de foobar).

\end{itemize}

\section{Votre plan}
    \label{sec:plan}

Le plan de votre mémoire est libre, il ne doit pas forcément être
structuré suivant les items ci-dessus, mais il doit faire apparaître
clairement votre démarche et les points évoqués ci-dessus. Certains
(par exemple la mise en perspective) peuvent faire l'objet de quelques
paragraphes dans la conclusion. Les enchaînements entre les chapitres
et les sections doivent être compréhensibles.

N'omettez pas la table des matières, et numérotez vos chapitres et
vos sections (1, 1.1, 1.1.1, etc.), cela aide à se repérer. Et bien
évidemment, numérotez les pages, sinon ça ne sert à rien.

\section {L'introduction et la conclusion}

L'introduction et la conclusion sont des parties très sensibles de votre
mémoire. La première doit susciter l'intérêt du lecteur et le motiver
pour lire votre document. Ceci passe par quelques figures imposées :

\begin {itemize}
    \item rappeler le problème à résoudre : vous devez montrer
	son intérêt, par exemple avec les applications pratiques qui
	peuvent en découler~;

    \item évoquer l'intérêt de lire le mémoire, et ce qu'on va
	apprendre en le lisant~;

    \item identifier votre contribution personnelle~: même si celle-ci
	vous semble modeste, il est important de la clarifier~;

    \item détailler le plan que vous allez suivre~: en donnant un sens
	à votre mémoire, vous racontez une «~histoire~» au lecteur
	et vous le motivez pour lire jusqu'au bout.

\end {itemize}

Un mauvais réflexe, malheureusement trop courant, consiste à conserver
le suspense jusqu'au bout et ne rien dévoiler avant le chapitre 2865
qui va enfin révolutionner la science. Il ne faut surtout pas faire
ainsi ! Vous aurez perdu le lecteur bien avant... Au contraire, il faut
clairement annoncer dès le début ce que vous avez fait. Votre tâche
n'est pas d'écrire un roman policier, mais un mémoire qui est votre
seul ambassadeur auprès du rapporteur chargé d'évaluer votre travail.

La conclusion doit elle aussi passer par des figures imposées :
\begin{itemize}
    \item rappeler très brièvement le problème à résoudre ;
    \item rappeler la contribution, en synthétisant ce que vous avez
	écrit dans le corps de votre mémoire ;
    \item évoquer des perspectives pour prolonger le travail :
	ce faisant, vous montrerez votre recul sur le sujet.
\end{itemize}

\section{Le plagiat}

\begin{figure}[htbp]
    \begin{center}
	\includegraphics[width=0.8\linewidth]{choc-nobel}
    \end{center}
    \caption{Corrélations entre pays suivant le nombre de lauréats du
	prix Nobel pour 10 millions d'habitants et (A) consommation
	de thé par habitant, (B) consommation de vin par habitant,
	(C) nombre de magasins Ikea pour 10 millions d'habitants et (D)
	PIB par habitant. Corrélation entre pays (E) suivant la
	consommation de chocolat et le PIB par habitant
	(extrait de \cite{maurage2013})}
    \label{fig:choc-nobel}
\end{figure}

Il est rappelé que le plagiat~\cite{bu2020} est sanctionné par la loi, par
l'université et par vos rapporteurs : si les courtes citations sont
autorisées (une ou deux phrases maximum, quelques figures difficiles
à reproduire comme par exemple la figure~\ref{fig:choc-nobel} extraite
de \cite{maurage2013}), vous devez en donner la source.

Par le passé, des plagiats ont été constatés comme par exemple
avec des mémoires comprenant des paragraphes entiers traduits et/ou
paraphrasés à partir d'un ou plusieurs articles (cités ou non).
Traduits devant le conseil de discipline de l'université, leurs auteurs
ont été sévèrement sanctionnés. Le jury est souvent plus clément
pour quelqu'un qui reconnaît le manque de travail effectué que pour
une personne dont le mémoire est entaché par un plagiat.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapitre 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{La forme}
    \label{chap:forme}

Un bon mémoire allie un fond de qualité et une forme parfaite. Quelques
règles de bon sens s'appliquent.

\section{Taille}

Votre mémoire doit faire 20 pages, annexes non comprises.

Les annexes sont exclues de la taille car leur lecture ne doit pas être
nécessaire pour évaluer votre mémoire et votre travail : elles ne
sont utiles que pour les personnes intéressées pour approfondir. En
tout état de cause, vous devez limiter leur taille.


\section{Typographie, orthographe et grammaire}

La rédaction de textes français obéit à des règles précises en
Français : cela s'appelle la «~typographie~» \cite{andre1990} et il
est intéressant de s'en imprégner pour éviter des erreurs grossières
et donner à votre document un aspect de qualité.

L'orthographe et la grammaire sont des prérequis indispensables pour
la rédaction du mémoire. Si vous n'êtes pas sûr de vous, faites-vous
relire par un tiers : il est dommage de perdre des points sur ce critère.

\section{Style}

Même si votre mémoire n'a pas comme objectif de décrocher le prix
Goncourt, vous devez faire attention au style :

\begin{itemize}
    \item faites des phrases construites, avec des verbes. Exemple vécu,
	à éviter : «~Il y a des problèmes. Par exemple le
	format.~» (il n'y pas de verbe)~;

    \item ne parlez pas au lecteur. Exemple vécu, à éviter : «~je vais
	vous présenter~»~;

    \item employez une forme active : plutôt qu'écrire «~xyz a été
	réalisé~», utilisez «~j'ai réalisé xyz~»~;

    \item soyez précis : écrivez «~j'ai réalisé xyz~» plutôt que «~on a
	réalisé xyz~». N'oubliez pas que le rapporteur doit évaluer
	\textbf{votre} travail (et pas celui de votre encadrant ou
	d'autres auteurs)~;

    \item évitez le futur : tout ce qui est fait au moment de l'écriture
	du mémoire doit être rédigé au passé ou au
	présent. Réservez le futur pour ce qui n'est pas encore
	réalisé ou pour les perspectives.

\end{itemize}


\section{Numérotez}

Numérotez tout ce qui peut l'être : pages, chapitres, sections, figures,
tables, bibliographie. Laissez à votre logiciel le soin de numéroter
automatiquement, il le fera mieux que vous «~manuellement~». Utilisez
des références si vous devez mettre en relation plusieurs éléments
de votre discours (exemple fictif : voir la figure~\ref{fig:choc-nobel},
page~\pageref{fig:choc-nobel}, ou encore le chapitre~\ref{chap:plan} et
plus spécifiquement la section~\ref{sec:plan}, page~\pageref{sec:plan}).


\section{Illustrations}

Un petit dessin valant mieux qu'un grand discours, les schémas sont
très appréciés par le rapporteur qui ne connaît pas forcément
votre contexte aussi bien que vous. Quelques règles cependant sont à
respecter :

\begin{itemize}
    \item numérotez et légendez vos figures (voir figure~\ref
	{fig:choc-nobel}, page~\pageref{fig:choc-nobel}, par exemple) ;
    \item référencez vos figures : une figure non référencée dans le
	texte ne sert à rien ;
    \item expliquez vos figures dans le texte : si une figure ne nécessite
	pas d'explications, cela signifie qu'elle est sans doute trop
	simple et n'a donc pas besoin de figurer dans votre mémoire ;
    \item la bonne taille pour les figures est celle où les caractères
	sont à peu près de la même taille que le reste du texte~;
    \item citez la provenance de vos figures, si elles ne sont pas de
	vous : il est admis d'utiliser des figures réalisées par
	d'autres, notamment des figures complexes, si vous citez en la
	provenance ;
    \item les logos de logiciels, d'outils, de marques ou de produits
	sont à bannir : ils n'apportent rien à la compréhension de
	votre mémoire et ne font que prendre de la place inutile que
	vous pourriez utiliser pour mieux présenter votre contribution ;
    \item privilégiez (sauf peut-être pour les photos) un format
	«~vectoriel~» à un format «~bitmap~» : le format bitmap prend
	de la place, peut être lent à s'afficher ou à s'imprimer,
	et ne permet pas de zoomer facilement ;
    \item vérifiez que vos figures sont lisibles lorsque vous imprimez
	votre mémoire, y compris lorsque vous imprimez sur une
	imprimante noir et blanc.
\end{itemize}

Les copies d'écran n'apportent généralement pas grand-chose : elles
contiennent trop d'informations inutiles et sont peu lisibles. De plus,
le message véhiculé est souvent très succinct, voire trop léger. Un
fichier de configuration, une commande ou son résultat doivent être
présentés, si c'est vraiment nécessaire, comme du texte et non comme
une copie d'écran.

\section{Niveau de détail}

Trouver le bon niveau de détail est souvent facilité par les contraintes
qui vous sont imposées quant au nombre de pages.

Cependant, même si vous avez la place, évitez les recopies de code ou de
commandes. Si vous voulez vraiment en faire, commentez-les dans le texte
et évitez de faire des erreurs dans les recopies ! Ne les modifiez pas
dans le texte (sauf pour supprimer une sortie trop longue par exemple,
dans ce cas remplacez la partie supprimée par «~[...]~»). Évitez
les codes trop longs~: utilisez des annexes si nécessaire.


\section{Bibliographie}

La bibliographie constitue une partie importante de votre mémoire,
car c'est la base sur laquelle se construisent les avancées
scientifiques. Elle constitue un critère de qualité du travail
(avez-vous trouvé les bonnes sources ? les documents sur lesquels vous
vous appuyez sont-ils sérieux ?).

La bibliographie~\cite{savoirs2010} vient en annexe, elle doit donner
tous les renseignements nécessaires pour permettre au lecteur de
retrouver les documents concernés : auteur, titre du document ou de
l'ouvrage, éditeur, année de publication, URL si nécessaire, date de
consultation pour un site Web, etc.

Chaque document dans la bibliographie comporte une référence (un
numéro, une abréviation ou autre), que vous devez citer dans le texte :
un document non cité ne devrait pas apparaître dans la bibliographie.


\section{Modèle \LaTeX}

Si vous rédigez votre mémoire avec \LaTeX{}, vous pouvez si vous le
souhaitez utiliser le source\footnote{Disponible sur
\url{https://gitlab.com/pdagog/ens/-/tree/master/mem-ter}.} de ce
document comme modèle pour votre propre mémoire.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Conclusion}
    \label{chap:conc}

Il est maintenant temps de rédiger votre mémoire. Puissent ces quelques
conseils vous guider afin de vous aider à présenter le mieux possible
tout le travail que vous avez effectué durant votre TER !


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Annexe : critères d'évaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
\chapter{Critères d'évaluation}
    \label{anx:crit-eval}

\vspace*{-20mm}	% on triche

\begin{center}
    \includegraphics[height=.8\textheight]{crit-eval.pdf}

    \vspace* {-20mm} % on triche encore

    Source : Pascal Schreck
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Annexe : soutenance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
\chapter {La soutenance}

Même si ce document concerne en priorité votre mémoire de stage,
il n'est pas inutile de rappeler quelques conseils de bon sens
pour que votre soutenance se déroule le mieux possible.

\begin {enumerate}
    \item Ne reproduisez pas le mémoire dans votre présentation : vous
	n'avez ni la place, ni le temps. Détachez-vous du mémoire et
	repartez de zéro pour construire un nouveau discours tenant
	compte de la contrainte de temps.

    \item Travaillez sur les idées et les messages que vous voulez
	faire passer. Comptez une idée par diapo. Explicitez les idées,
	ne vous contentez pas de les suggérer.

    \item Ne surchargez pas le texte de votre présentation : ne faites
	pas de phrases, insistez plutôt sur quelques mots pour exposer
	vos idées.

    \item Si vous pouvez prendre des libertés avec la grammaire et
	ne pas mettre de phrases, vous n'êtes pas dispensé de respecter
	l'orthographe.

    \item Adoptez un fond sobre pour ne pas perturber votre message.
	Numérotez vos diapos.

    \item Faites des illustrations (schémas, figures, courbes) qui
	puissent être lues à plusieurs mètres de distance. N'hésitez
	pas à prendre des libertés avec votre style de présentation
	pour faire une figure en pleine page.

    \item Attention aux contrastes : votre présentation projetée dans
	une salle éclairée aura un contraste beaucoup moins bon que
	l'écran de votre portable. Évitez donc les couleurs pâles
	sur fond clair, ou les couleurs peu foncées sur fond sombre.

    \item Une de vos missions est de maintenir l'attention de votre
	auditoire. Pensez que les membres du jury ont déjà peut-être
	une dizaine de présentations à leur actif, ainsi qu'un bon
	repas... Vous devez les motiver pour vous écouter.

    \item Ne lisez surtout pas les diapos que vous présentez ou, pire
	encore, un texte que vous auriez préparé. Regardez l'assistance
	et non vos diapos.

    \item Respectez la durée de votre présentation, voir plus loin.

    \item Répétez. Répétez. Répétez. Répétez. Répétez. Répétez.
	Répétez. Répétez. Répétez. Répétez. Répétez. Répétez.

    \item Lors de la séance des questions, laissez les membres du jury
	aller jusqu'au bout de leurs questions, sans les
	interrompre. N'hésitez pas à prendre quelques secondes pour
	vous permettre de réfléchir à chaque question, voire de la
	reformuler pour vérifier que vous l'avez bien comprise.

\end {enumerate}

Si vous n'avez pas l'habitude de faire des présentations, vous trouverez
facilement un grand nombre de vidéos ou de tutoriels qui vous donneront
de bons conseils.

Les directives qui vous sont données pour la soutenance incluent la
durée de la présentation. Vous devez impérativement respecter cette
durée~: ne terminez pas trop en avance (vous n'avez donc rien à dire ?),
ne terminez pas trop en retard (vous ne savez pas synthétiser et tenir
compte d'une contrainte ?). Il arrive très souvent qu'il y ait un barème
pour la durée, comme l'illustre la figure~\ref{fig:duree-prez}~: par
exemple, si la consigne est une durée de 20 minutes, vous aurez 4 points
sur 4 si votre présentation dure entre 19' et 20'59", 3 points sur 4
si votre présentation dure entre 18' et 18'59" ou entre 21' et 21'29",
etc. Si vous dépassez trop, le président du jury coupera court à
votre présentation.

\begin {figure} [htbp]
    \label {fig:duree-prez}
    \begin {center}
	\includegraphics [width=\textwidth] {duree-prez.pdf}
    \end {center}
    \caption {Barème de la note (ici sur 4) en fonction de la durée
	de la présentation}
\end {figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliographie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\cleardoublepage
\bibliographystyle{plain}
\bibliography{mem-ter}

\end{document}
